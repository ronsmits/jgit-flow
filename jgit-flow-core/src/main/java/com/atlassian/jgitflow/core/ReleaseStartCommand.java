package com.atlassian.jgitflow.core;

import com.atlassian.jgitflow.core.exception.*;
import com.atlassian.jgitflow.core.util.GitHelper;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.util.StringUtils;

import static com.atlassian.jgitflow.core.util.Preconditions.checkState;

/**
 * Start a release.
 * <p>
 * This will create a new branch using the release prefix and release name from the tip of develop
 * </p>
 * <p>
 * Examples (<code>flow</code> is a {@link JGitFlow} instance):
 * <p>
 * Start a feature:
 *
 * <pre>
 * flow.releaseStart(&quot;1.0&quot;).call();
 * </pre>
 * <p>
 * Perform a fetch of develop before branching
 *
 * <pre>
 * flow.releaseStart(&quot;1.0&quot;).setFetch(true).call();
 * </pre>
 */
public class ReleaseStartCommand extends AbstractGitFlowCommand<Ref>
{
    //TODO: add ability to pass in start commit on ALL commands
    private final String releaseName;
    private boolean fetch;

    /**
     * Create a new release start command instance.
     * <p></p>
     * An instance of this class is usually obtained by calling {@link JGitFlow#releaseStart(String)}
     * @param releaseName The name of the release
     * @param git The git instance to use
     * @param gfConfig The GitFlowConfiguration to use
     */
    public ReleaseStartCommand(String releaseName, Git git, GitFlowConfiguration gfConfig)
    {
        super(git, gfConfig);

        checkState(!StringUtils.isEmptyOrNull(releaseName));
        this.releaseName = releaseName;
        this.fetch = false;
    }

    /**
     * 
     * @return A reference to the new release branch
     * @throws NotInitializedException
     * @throws JGitFlowGitAPIException
     * @throws ReleaseBranchExistsException
     * @throws DirtyWorkingTreeException
     * @throws JGitFlowIOException
     * @throws LocalBranchExistsException
     * @throws TagExistsException
     * @throws BranchOutOfDateException
     */
    @Override
    public Ref call() throws NotInitializedException, JGitFlowGitAPIException, ReleaseBranchExistsException, DirtyWorkingTreeException, JGitFlowIOException, LocalBranchExistsException, TagExistsException, BranchOutOfDateException
    {
        String prefixedReleaseName = gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.RELEASE.configKey()) + releaseName;

        requireGitFlowInitialized();
        requireNoExistingReleaseBranches();
        requireCleanWorkingTree();
        requireLocalBranchAbsent(prefixedReleaseName);

        try
        {
            if (fetch)
            {
                git.fetch().call();
            }

            requireTagAbsent(gfConfig.getPrefixValue(JGitFlowConstants.PREFIXES.VERSIONTAG.configKey()) + releaseName);

            if (GitHelper.remoteBranchExists(git, gfConfig.getDevelop()))
            {
                requireLocalBranchNotBehindRemote(gfConfig.getDevelop());
            }

            RevCommit developCommit = GitHelper.getLatestCommit(git, gfConfig.getDevelop());

            return git.checkout()
                      .setName(prefixedReleaseName)
                      .setCreateBranch(true)
                      .setStartPoint(developCommit)
                      .call();

        }
        catch (GitAPIException e)
        {
            throw new JGitFlowGitAPIException(e);
        }
    }

    /**
     * Set whether to perform a git fetch of the remote develop branch before branching
     * @param fetch
     *              <code>true</code> to do the fetch, <code>false</code>(default) otherwise
     * @return {@code this}
     */
    public ReleaseStartCommand setFetch(boolean fetch)
    {
        this.fetch = fetch;
        return this;
    }
}
